import React from 'react';
import { THeader } from './types/header-type';

export const Header = ({componentName}: THeader) => {
  return (
    <div className="header">
      <div className="text">{componentName}</div>
    </div>
  );
}
