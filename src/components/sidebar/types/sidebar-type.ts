import { Dispatch, SetStateAction } from "react";

export type TSidebar = {
  setComponentName: Dispatch<SetStateAction<string>>;
};
