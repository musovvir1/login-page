import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Routes } from '../../constants/routes';

const Home = lazy(() => import('../../home/home'));
const Messages = lazy(() => import('../../messages/messages'));
const FileManager = lazy(
  () => import('../../file-manager/file-manager')
);
const Dashboard = lazy(() => import('../../dashboard/dashboard'));
const Contacts = lazy(() => import('../../contacts/contacts'));
const User = lazy(() => import('../../user/user'));

const SidebarRoutes = () => {
  return (
    <div>
      <Suspense fallback={<h1 className="loading">Загрузка данных...</h1>}>
        <Switch>
          <Route path={Routes.home} component={Home} />
          <Route path={Routes.messages} component={Messages} />
          <Route path={Routes.files} component={FileManager} />
          <Route path={Routes.contacts} component={Contacts} />
          <Route path={Routes.dashboard} component={Dashboard} />
          <Route path={Routes.user} component={User} />
        </Switch>
      </Suspense>
    </div>
  );
};

export default SidebarRoutes;
