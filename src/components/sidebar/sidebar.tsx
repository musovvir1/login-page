import React, { useState } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { useLocalStorageHook } from "../../hooks/local-storage-hook/use-local-storage-hook";
import { useUserIsLoggedInHook } from "../../hooks/local-storage-hook/use-user-is-logged-in-hook";
import { Button } from "../button/button";
import { Routes } from "../constants/routes";
import { ShowSidebar } from "../show-sidebar/show-sidebar";
import { TSidebar } from "./types/sidebar-type";

export const Sidebar = ({ setComponentName }: TSidebar) => {
  const [showSidebar, setShowSidebar] = useState<boolean>(false);
  const { removeUser, checkAdmin } = useLocalStorageHook();
  const isAdmin = checkAdmin("user-logged");

  const hideSidebar = () => {
    setShowSidebar(!showSidebar);
  };

  const logout = () => {
    removeUser("user-logged");
  };

  const getComponentName = (e: React.MouseEvent<HTMLElement>) => {
    setComponentName(e.currentTarget.textContent as string);
  };

  return (
    <div className={`sidebar ${showSidebar ? "sidebar-active" : ""}`}>
      <ShowSidebar onClick={hideSidebar} />
      <ul>
        <li className="home-page">
          <NavLink
            to={Routes.home + "/tab-one"}
            activeClassName="nav-active"
            onClick={getComponentName}
          >
            <span className="common">
              <i className="bx bxs-home" />
              <span className="sidebar-text">Home</span>
            </span>
          </NavLink>

          <span className="tooltip">Home</span>
        </li>
        <li className="messages">
          <NavLink
            to={Routes.messages}
            activeClassName="nav-active"
            onClick={getComponentName}
          >
            <span className="common">
              <i className="bx bxs-chat" />
              <span className="sidebar-text">Messages</span>
            </span>
          </NavLink>
          <span className="tooltip">Messages</span>
        </li>
        <li className="file-manager">
          <NavLink
            to={Routes.files}
            activeClassName="nav-active"
            onClick={getComponentName}
          >
            <span className="common">
              <i className="bx bxs-file-blank" />

              <span className="sidebar-text">File Manager</span>
            </span>
          </NavLink>
          <span className="tooltip">Files</span>
        </li>
        <li className="contacts">
          <NavLink
            to={Routes.contacts}
            activeClassName="nav-active"
            onClick={getComponentName}
          >
            <span className="common">
              <i className="bx bxs-phone" />
              <span className="sidebar-text">Contacts</span>
            </span>
          </NavLink>
          <span className="tooltip">Contacts</span>
        </li>
        <li className="dashboard">
          <NavLink
            to={Routes.dashboard}
            activeClassName="nav-active"
            onClick={getComponentName}
          >
            <span className="common">
              <i className="bx bxs-dashboard" />
              <span className="sidebar-text">Dashboard</span>
            </span>
          </NavLink>
          <span className="tooltip">Dashboard</span>
        </li>
       
        {isAdmin && (
          <li className="user">
            <NavLink
              to={Routes.user}
              activeClassName="nav-active"
              onClick={getComponentName}
            >
              <span className="common">
                <i className="bx bxs-user" />
                <span className="sidebar-text">User</span>
              </span>
            </NavLink>
            <span className="tooltip">User</span>
          </li>
        )}
      </ul>
      <Button
        type="reset"
        className="btnLogout"
        text="Выход"
        disabled
        onClick={logout}
      />
    </div>
  );
};
