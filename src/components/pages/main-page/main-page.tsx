import React, { useState } from 'react';
import { Footer } from '../../footer/footer';
import { Header } from '../../header/header';
import { Sidebar } from '../../sidebar/sidebar';
import MainPageRoutes from '../main-page-routes/main-page-routes';

const MainPage = () => {
  const [componentName, setComponentName] = useState<string>('Home');
  return (
    <div className="main-page">
      <Header componentName={componentName} />
      <Sidebar setComponentName={setComponentName} />
      <MainPageRoutes />
      <Footer />
    </div>
  );
};

export default MainPage;
