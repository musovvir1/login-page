import { useFormik } from 'formik';
import * as Yup from 'yup';
import React, { useEffect, useState } from 'react';
import { useIdentificationHook } from '../../../hooks/identification-hook/use-identification-hook';
import { Button } from '../../button/button';
import TextField from '../../text-field/text-field';
import { TFormik } from './types/formik-type';

const LoginPage = () => {
  const [formValidate, setFormValidate] = useState<boolean>(false);

  const formik = useFormik<TFormik>({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .max(250, 'Email должен быть менее 250 символов')
        .email('Некорректный Email')
        .required('Email не может быть пустым'),
      password: Yup.string()
        .matches(/\d{4}/, 'Пароль должен содержать минимум 4 цифры')
        .matches(
          /[A-Z]/,
          'Пароль должен содержать только латинские буквы и 1 букву верхнего регистра'
        )
        .matches(
          /[!@#$%^&*()\-=_+~[\]{}'"\\|../<>?]{2}/,
          'Пароль должен содержать минимум 2 спец-символа'
        )
        .min(12, 'Пароль должен быть минимум 12 символов')
        .max(250, 'Пароль должен быть менее 250 символов')
        .required('Пароль не может быть пустым'),
    }),
    onSubmit: () => {
      handleSubmit();
    },
  });

  const { handleSubmit, error } = useIdentificationHook({
    formik,
  });

  useEffect(() => {
    if (formik.errors.email || formik.errors.password) {
      setFormValidate(false);
    } else {
      setFormValidate(true);
    }
  }, [formik.errors.email, formik.errors.password]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="container">
        <h1 className="authorizing">Авторизация</h1>
        <div className="commonInput">
          <h2 className="login">Login</h2>
          {formik.errors.email && formik.touched.email && (
            <div className="errorLogin">{formik.errors.email}</div>
          )}
          <div className="errorLogin">{error}</div>
          <TextField
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="email"
            placeholder="Введите email"
            type="text"
            className="inputText"
          />
          <h2 className="password">Password</h2>
          {formik.errors.password && formik.touched.password && (
            <div className="errorPassword">{formik.errors.password}</div>
          )}
          <TextField
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            name="password"
            placeholder="Введите пароль"
            type="password"
            className="inputText"
          />
        </div>
        <div className="buttons">
          <Button
            type="submit"
            disabled={formValidate}
            text="Отправить"
            className="btnSubmit"
          />
          <Button
            type="reset"
            disabled={formValidate}
            text="Очистить"
            className="btnClear"
          />
        </div>
      </div>
    </form>
  );
};

export default LoginPage;
