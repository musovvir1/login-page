import React from 'react';
import { TShowSidebar } from './types/show-sidebar-type';

export const ShowSidebar = ({onClick}: TShowSidebar) => {
 
  return (
    <div className="show-sidebar" onClick={onClick}>
      <i className='bx bx-menu' />
    </div>
  );
}