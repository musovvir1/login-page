import React from "react";
import { Route } from "react-router-dom";
import { TDataItem } from "../../tabs/types/data-item-type";
import { TData } from "../../tabs/types/data-type";
import { HomeContent } from "../home-content/home-content";

const HomeRoutes = ({ data }: TDataItem) => {
  return (
    <div>
      {data.map((item: TData, index: number) => {
        return (
          <Route
            key={index}
            path={item.path}
            render={() => (
              <HomeContent
                toggleState={item.toggleState}
                title={item.title}
                text={item.text}
              />
            )}
          />
        );
      })}
    </div>
  );
};

export default HomeRoutes;
