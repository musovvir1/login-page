export type THomeContent = {
    toggleState: number;
    title: string;
    text: string;
  };
  