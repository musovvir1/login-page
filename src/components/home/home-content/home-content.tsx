import { THomeContent } from "../types/home-content-type";

export const HomeContent = ({ toggleState, title, text }: THomeContent) => {
  return (
    <div className="content-home">
      <div
        className={toggleState === 1 ? "content  active-content" : "content"}
      >
        <h2>{title}</h2>
        <hr />
        <p>{text}</p>
      </div>
    </div>
  );
};
