import React from "react";
import { useLocalStorageHook } from "../../hooks/local-storage-hook/use-local-storage-hook";
import { tabs } from "../constants/tabs";
import { Tabs } from "../tabs/tabs";
import HomeRoutes from "./home-routes/home-routes";

const Home = () => {
  const { checkAdmin } = useLocalStorageHook();

  const isAdmin = checkAdmin("user-logged");

  return (
    <div className="home-container">
      <Tabs data={tabs} isAdmin={isAdmin}/>
      <HomeRoutes data={tabs}/>
    </div>
  );
};

export default Home;
