export type TButtonProps = {
  type: 'button' | 'submit' | 'reset' | undefined;
  disabled?: boolean | undefined;
  onClick?: () => void;
  text: string;
  className: string;
};
