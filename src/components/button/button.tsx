import React from 'react';
import { TButtonProps } from './types/button-props-type';

export const Button = ({
  type,
  disabled,
  onClick,
  text,
  className
}: TButtonProps) => {
  return (
      <button type={type} disabled={!disabled} className={className} onClick={onClick}>
        {text}
      </button>
  );
};
