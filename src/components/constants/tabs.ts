import { TData } from "../tabs/types/data-type";
import { Routes } from "./routes";

export const tabs: TData[] = [
  {
    isAdmin: false,
    tabName: "Tab-One",
    className: "tabs",
    activeClassName: "active-tabs",
    path: Routes.tabOne,
    title: "Content 1",
    text: "This is the first tab",
    toggleState: 1,
  },
  {
    isAdmin: false,
    tabName: "Tab-Two",
    className: "tabs",
    activeClassName: "active-tabs",
    path: Routes.tabTwo,
    title: "Content 2",
    text: "This is the second tab",
    toggleState: 2,
  },
  {
    isAdmin: false,
    tabName: "Tab-Three",
    className: "tabs",
    activeClassName: "active-tabs",
    path: Routes.tabThree,
    title: "Content 3",
    text: "This is the third tab",
    toggleState: 3,
  },
  {
    isAdmin: true,
    tabName: "Tab-Admin",
    className: "tabs",
    activeClassName: "active-tabs",
    path: Routes.tabAdmin,
    title: "Admin",
    text: "This is the admin tab",
    toggleState: 4,
  },
];
