export type TTab = {
  path: string;
  className: string;
  activeClassName: string;
  tabName: string;
};
