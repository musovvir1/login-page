export type TData = {
  isAdmin: boolean;
  path: string;
  className: string;
  activeClassName: string;
  tabName: string;
  toggleState: number;
  title: string;
  text: string;
};

