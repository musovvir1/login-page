import { TData } from "./data-type";

export type TDataItem = {
  data: TData[];
  isAdmin?: boolean;
};
