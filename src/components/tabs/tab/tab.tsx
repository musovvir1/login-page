import { NavLink } from "react-router-dom";
import { TTab } from "../types/tab-type";

export const Tab = ({ path, className, activeClassName, tabName }: TTab) => {
  return (
    <NavLink to={path} activeClassName={activeClassName} className={className}>
      {tabName}
    </NavLink>
  );
};
