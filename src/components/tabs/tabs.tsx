import { Tab } from "./tab/tab";
import { TDataItem } from "./types/data-item-type";
import { TData } from "./types/data-type";

export const Tabs = ({ data, isAdmin }: TDataItem) => {
  return (
    <div className="bloc-tabs">
      {data.map((tab: TData, index: number) => {
        if (tab.isAdmin && !isAdmin) {
          return false;
        } else {
          return (
            <Tab
              key={index}
              path={tab.path}
              className={tab.className}
              activeClassName={tab.activeClassName}
              tabName={tab.tabName}
            />
          );
        }
      })}
    </div>
  );
};
