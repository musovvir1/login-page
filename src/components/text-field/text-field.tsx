import React from 'react';
import { TTextField } from './types/text-field-type';

export const TextField = ({
  name,
  placeholder,
  type,
  value,
  onChange,
  onBlur,
  className
}: TTextField) => {
 

  return (
    <div>
      <input
        name={name}
        placeholder={placeholder}
        type={type}
        onBlur={onBlur}
        value={value}
        onChange={onChange}
        className={className}
      />
    </div>
  );
}

export default TextField;
