export type TTextField = {
  name: string;
  value: string;
  onChange: (e: React.FocusEvent<HTMLInputElement>) => void;
  onBlur: (e: React.FocusEvent<HTMLInputElement>) => void;
  type: string;
  placeholder: string;
  className: string
};