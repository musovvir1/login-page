import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Routes } from '../../components/constants/routes';
import { TLocal } from './types/local-storage-type';
import { TUserLogged } from './types/user-logged-type';

export const useLocalStorageHook = (): TUserLogged => {
  const [stateWithLocal, setStateWithLocal] = useState<string | null | void>(
    null
  );

  const history = useHistory();

  const localUserLogged = (key: string) => {
    return localStorage.getItem(key);
  };

  const localUserIsLoggedIn = (key: string) => {
    if (localUserLogged(key)) {
      setStateWithLocal(localStorage.getItem(key));
    }
  };

  const setUserLogged = (key: string, value: TLocal) => {
    localStorage.setItem(key, JSON.stringify(value));
  };

  const checkAdmin = (key: string) => {
    const getUserIsAdmin = localStorage.getItem(key);
    const isAdmin = !!getUserIsAdmin && JSON.parse(getUserIsAdmin);
    return isAdmin.isAdmin;
  };

  const removeUser = (key: string) => {
    if (localUserLogged(key)) {
      setStateWithLocal(localStorage.removeItem(key));
      history.push(Routes.loginPage);
    }
  };

  return {
    stateWithLocal,
    localUserIsLoggedIn,
    setUserLogged,
    removeUser,
    checkAdmin,
  };
};
