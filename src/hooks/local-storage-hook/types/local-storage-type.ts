export type TLocal = {
  email: string | boolean;
  isAdmin: boolean;
};
