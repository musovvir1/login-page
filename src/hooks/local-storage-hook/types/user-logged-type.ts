import { TLocal } from './local-storage-type';

export type TUserLogged = {
  stateWithLocal: string | null | void;
  localUserIsLoggedIn: (key: string) => void;
  setUserLogged: (key: string, value: TLocal) => void;
  removeUser: (key: string) => void;
  checkAdmin: (key: string) => boolean;
};
