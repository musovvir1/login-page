import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Routes } from "../../components/constants/routes";
import { TUserIsLoggedIn } from "./types/user-is-logged-in-type";
import { useLocalStorageHook } from "./use-local-storage-hook";

export const useUserIsLoggedInHook = (): TUserIsLoggedIn => {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

  const { stateWithLocal, localUserIsLoggedIn } = useLocalStorageHook();

  const history = useHistory();

  useEffect(() => {
    localUserIsLoggedIn("user-logged");
  }, [localUserIsLoggedIn]);

  useEffect(() => {
    if (stateWithLocal) {
      setIsLoggedIn(true);
      history.replace(Routes.home + "/tab-one");
    } else {
      setIsLoggedIn(false);
      history.replace(Routes.loginPage);
    }
  }, [history, stateWithLocal]);

  const location = useLocation()

  useEffect(() => {
    return history.listen(() => {
      if (history.action === "POP") {
        if (!isLoggedIn) {
          history.replace(Routes.loginPage);
        }
      }
    });
  }, [history, isLoggedIn, location]);

  useEffect(() => {
    if(location.pathname === Routes.loginPage) {
      setIsLoggedIn(false)
    }
  }, [location.pathname]);

  return {
    isLoggedIn,
  };
};
