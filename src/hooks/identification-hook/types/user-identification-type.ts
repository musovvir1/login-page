import { FormikProps } from "formik";
import { TFormik } from "../../../components/pages/login-page/types/formik-type";

export type TUserIdentification = {
  formik: FormikProps<TFormik>;
};
