export type THandle = {
  handleSubmit: () => void;
  error: string;
};
