import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Routes } from "../../components/constants/routes";
import { usersData } from "../../components/constants/users-data";
import { useLocalStorageHook } from "../local-storage-hook/use-local-storage-hook";
import { THandle } from "./types/handle-click-type";
import { TUserIdentification } from "./types/user-identification-type";

export const useIdentificationHook = ({
  formik,
}: TUserIdentification): THandle => {
  const history = useHistory();

  const { setUserLogged } = useLocalStorageHook();

  const [error, setError] = useState<string>("");

  const users = usersData.find(
    (user) =>
      user.email === formik.values.email &&
      user.password === formik.values.password
  );

  const handleSubmit = (): void => {
    if (users) {
      setUserLogged("user-logged", {
        email: formik.values.email,
        isAdmin: users.isAdmin,
      });
      history.replace(Routes.home + "/tab-one");
    } else {
      setError("Неправильный логин или пароль");
    }
  };

  return {
    handleSubmit,
    error,
  };
};
