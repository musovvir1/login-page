import { Route, Switch } from 'react-router-dom';
import { Routes } from './components/constants/routes';
import { useUserIsLoggedInHook } from './hooks/local-storage-hook/use-user-is-logged-in-hook';
import './App.scss';
import LoginPage from './components/pages/login-page/login-page';
import MainPage from './components/pages/main-page/main-page';

export const App = () => {
  useUserIsLoggedInHook();

  return (
    <div className="app">
        <Switch>
          <Route exact path={Routes.loginPage} component={LoginPage} />
          <Route path={Routes.mainPage} component={MainPage} />
        </Switch>
    </div>
  );
};
